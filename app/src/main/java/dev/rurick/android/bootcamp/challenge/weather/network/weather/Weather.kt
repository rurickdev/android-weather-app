package dev.rurick.android.bootcamp.challenge.weather.network.weather

import java.io.Serializable

data class Weather(
    val main: String,
    val description: String,
    val icon: String,
): Serializable
