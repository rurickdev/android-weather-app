package dev.rurick.android.bootcamp.challenge.weather.ui.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dev.rurick.android.bootcamp.challenge.weather.databinding.FragmentHomeErrorBinding

class HomeErrorFragment : Fragment(){
    private var _binding: FragmentHomeErrorBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeErrorBinding.inflate(inflater, container, false)
        return binding.root
    }
}