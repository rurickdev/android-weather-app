package dev.rurick.android.bootcamp.challenge.weather.network.weather

import dev.rurick.android.bootcamp.challenge.weather.network.city.CityEntity
import java.io.Serializable

data class OneCallEntity(
    val current: Current,
    val hourly: List<Current>,
    var city: CityEntity?
): Serializable
